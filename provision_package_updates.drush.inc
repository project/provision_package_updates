<?php

/**
 * @file
 * Provides a nightly update check for all packages that exist on a platform.
 */

define('PLATFORM_PACKAGE_UPDATES_BASE_URL', 'http://updates.drupal.org/release-history/');

/**
 * Implements hook_drush_command().
 */
function provision_package_updates_drush_command() {
  $items['provision-hosting_package_updates'] = array(
    'description' => 'Check for any updates available for an Aegir platform',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_ROOT,
  );

  return $items;
}

/**
 * Implements the provision-content_last_updated command.
 */
function drush_provision_package_updates_provision_hosting_package_updates() {
  drush_errors_on();

  if (d()->type === 'platform') {

    drush_include_engine('drupal', 'packages', drush_drupal_major_version());
    $scanned_packages = _provision_find_packages('sites', 'all');

    $found_packages = array();

    // De-dupe the found packages list
    foreach ($scanned_packages['modules'] as $name => $package) {
      if (!empty($package->project)) {
        $found_packages[$name] = $package;
      }
    }
    foreach ($scanned_packages['themes'] as $name => $package) {
      if (!empty($package->project)) {
        $found_packages[$name] = $package;
      }
    }

    $package_update_info = array();

    // Check for package updates
    if (!empty($found_packages)) {
      foreach ($found_packages as $name => $project) {
        if (!empty($project->project)) {
          $result = drush_provision_platform_package_updates_check_for_project_update($project);
          if (!empty($result)) {
            drush_log(dt('Available update information for @name: @information', array('@name' => $name, '@information' => print_r($result, TRUE))), 'success');
            $package_update_info[$name] = $result;
          }
        }
      }
    }

    // Check for core updates. We're going to fake the object here to avoid
    // special casing for core.
    $project = new stdClass();
    $project->project = 'drupal';
    $project->info = array();
    $project->version = VERSION;
    $version_parts = explode('.', $project->version);
    $project->info['core'] = $version_parts[0] . '.x';
    $result = drush_provision_platform_package_updates_check_for_project_update($project);
    if (!empty($result)) {
      drush_log(dt('Available update information for @name: @information', array('@name' => 'Drupal core', '@information' => print_r($result, TRUE))), 'success');
      $package_update_info['drupal'] = $result;
    }

    // Log a message and return this data to the frontend.
    drush_log(dt('Found @number packages that require updates.', array('@number' => count($package_update_info))), 'success');
    drush_set_option('package_updates', $package_update_info);
  }
}


/**
 * Update check function for a given project
 *
 * @param array project info parsed from .info file by _provision_find_packages().
 */
function drush_provision_platform_package_updates_check_for_project_update($project) {
  $update_base_url = isset($project->info['project status url']) ? $project->info['project status url'] : PLATFORM_PACKAGE_UPDATES_BASE_URL;

  if (!drush_cache_get(drush_get_cid('platform_package_updates-' . $project->project))) {
    $xml = file_get_contents($update_base_url . $project->project . '/' . $project->info['core']);

    // If we are requesting a project that doesn't exist, bail out now.
    if (strpos($xml, 'No release history was found for the requested project') !== FALSE) {
      drush_log(dt('Project was not found on Drupal.org. Skipping...'), 'notice');
      return array();
    }

    // XML is pretty much THE WORST format ever. See http://stackoverflow.com/a/18040685
    // for why we're doing this.
    $simplexml = simplexml_load_string($xml);
    $data = unserialize(serialize(json_decode(json_encode((array) $simplexml), 1)));

    drush_cache_set(drush_get_cid('platform_package_updates-' . $project->project), $data, 'default', DRUSH_CACHE_TEMPORARY);
  }
  else {
    $data = drush_cache_get(drush_get_cid('platform_package_updates-' . $project->project));
    // Drush 'helpfully' nests what you're trying to retrieve in an object.
    $data = $data->data;
  }

  drush_log(dt('Checking updates for: @project (current version: @version)', array('@project' => $project->project, '@version' => $project->version)), 'notice');

  // First, find the current release and get some additional data.
  $project_major_version = 0;
  $project_release_publish_date = 0;
  $project_recommended_major = $data['recommended_major'];
  $project_supported_majors = explode(',', $data['supported_majors']);

  foreach ($data['releases']['release'] as $release) {
    if ($release['version'] == $project->version) {
      $project_major_version = $release['version_major'];
      $project_release_publish_date = $release['date'];
    }
  }

  // Start from the beginning now that we have more data
  reset($data['releases']['release']);

  // Start counting these.
  $number_of_critical_updates = 0; // Security or recommended version difference
  $number_of_noncritical_updates = 0; // Anything else

  // First, is this project a nonsupported major version?
  if (!in_array($project_major_version, $project_supported_majors)) {
    $number_of_critical_updates++;
    drush_log(dt('Found critical update: unsupported major version in use.'), 'notice');
  }
  else {
    // So we have a supported major, but is it the *recommended* major? (if it's less, this is a noncritical)
    if ($project_major_version < $project_recommended_major) {
      $number_of_noncritical_updates++;
      drush_log(dt('Found noncritical update: recommended major version difference.'), 'notice');
    }
  }

  // Find any newer releases for the current recommended major version (not including the current version)
  foreach ($data['releases']['release'] as $release) {
    if (($release['version_major'] == $project_major_version) && ($release['version'] != $project->version) && ($release['date'] > $project_release_publish_date) && (!isset($release['version_extra']) || $release['version_extra'] != 'dev')) {

      // Given the conditions above, this will always happen.
      $number_of_noncritical_updates++;
      drush_log(dt('Found noncritical update: newer package for current major version available.'), 'notice');

      // If this release is a security update
      if (!empty($release['terms'])) {
        foreach ($release['terms'] as $term) {
          if (isset($term['value']) && stripos($term['value'], 'Security update') !== FALSE) {
            $number_of_critical_updates++;
            drush_log(dt("That's no noncritical! That's a SECURITY UPDATE!"), 'notice');
            drush_log(dt('Found critical update: SECURITY UPDATE available.'), 'notice');

            // If we have a critical update, we need one less noncritical update.
            $number_of_noncritical_updates--;
          }
        }
      }
    }
  }

  drush_log(dt('Found @critnumber critical updates and @noncritnumber noncritical updates for project: @project', array('@critnumber' => $number_of_critical_updates, '@noncritnumber' => $number_of_noncritical_updates, '@project' => $project->project)), 'notice');

  if ($number_of_noncritical_updates > 0 || $number_of_critical_updates > 0) {
    return array(
      'project' => $project->project,
      'critical_updates' => $number_of_critical_updates,
      'noncritical_updates' => $number_of_noncritical_updates
    );
  }

  // Otherwise, no data.
  return array();
}
